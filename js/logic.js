function init () {
  let today = new Date().toLocaleDateString()
  document.getElementById('fecha').value = today

  document.getElementById('tc').value = '4.0'
  document.getElementById('comision-deposito').value = '1'
  document.getElementById('comision-fija').value = '45'
  document.getElementById('comision-variable').value = '0.25'
}

function calcular (moneda) {
  /* Si no hay errores, procedemos a realizar el cálculo */
  if (validar() == 0) {
    let tc = parseFloat(document.getElementById('tc').value)
    let itf = 0.005
    let dep = parseFloat(document.getElementById('comision-deposito').value)
    let fija = parseFloat(document.getElementById('comision-fija').value)
    let vari = parseFloat(document.getElementById('comision-variable').value)
    let soles = parseFloat(document.getElementById('soles').value)
    let euros = parseFloat(document.getElementById('euros').value)

    let comDep = 0
    let comVar = 0
    let itflocal = 0
    let itfext = 0
    let aenviar = 0
    switch (moneda) {
      case 'soles':
        comDep = (dep / 100) * soles
        itflocal = (itf / 100) * soles
        soles -= comDep - itflocal

        aenviar = soles / tc

        comVar = (aenviar * vari) / 100
        if (comVar <= 25) {
          comVar = 25
        }

        itfext = (aenviar * itf) / 100
        euros = aenviar - fija - comVar - itfext

        document.getElementById('euros').value = euros.toFixed(2)
        document.getElementById(
          'comision-deposito-resutado'
        ).innerText = comDep.toFixed(2)
        document.getElementById(
          'comision-variable-resultado'
        ).innerText = comVar.toFixed(2)
        break
      case 'euros':
        aenviar = euros + fija / (1 + vari / 100 + itf / 100)
        comVar = (vari / 100) * aenviar
        if (comVar <= 25) {
          comVar = 25
          aenviar = (euros + fija + 25) / (1 - itf / 100)
        }

        soles = (aenviar * tc) / (1 - dep / 100 - itf / 100)
        comDep = (soles * dep) / 100

        document.getElementById('soles').value = soles.toFixed(2)
        document.getElementById(
          'comision-deposito-resutado'
        ).innerText = comDep.toFixed(2)
        document.getElementById(
          'comision-variable-resultado'
        ).innerText = comVar.toFixed(2)
        break
    }
  }
}

function validar () {
  let bad = 0
  let soles = document.getElementById('soles')
  if (isNaN(parseFloat(soles.value)) == true) {
    soles.classList.add('is-danger')
    bad++
  } else {
    soles.classList.remove('is-danger')
  }

  let euros = document.getElementById('euros')
  if (isNaN(parseFloat(euros.value)) == true) {
    euros.classList.add('is-danger')
    bad++
  } else {
    euros.classList.remove('is-danger')
  }

  let tc = document.getElementById('tc')
  if (isNaN(parseFloat(tc.value)) == true) {
    tc.classList.add('is-danger')
    bad++
  } else {
    tc.classList.remove('is-danger')
  }

  let comDep = document.getElementById('comision-deposito')
  if (isNaN(parseFloat(comDep.value)) == true) {
    comDep.classList.add('is-danger')
    bad++
  } else {
    comDep.classList.remove('is-danger')
  }

  let comFija = document.getElementById('comision-fija')
  if (isNaN(parseFloat(comFija.value)) == true) {
    comFija.classList.add('is-danger')
    bad++
  } else {
    comFija.classList.remove('is-danger')
  }

  let comVar = document.getElementById('comision-variable')
  if (isNaN(parseFloat(comVar.value)) == true) {
    comVar.classList.add('is-danger')
    bad++
  } else {
    comVar.classList.remove('is-danger')
  }

  let box = document.getElementById('box')
  let msg = document.getElementById('error')

  if (bad > 0) {
    let s = ''
    let ss = 'encontró'
    if (bad > 1) {
      s = 'es'
      ss = 'encontraron'
    }

    if (msg == undefined) {
      msg = document.createElement('article')
      msg.id = 'error'
      msg.className = 'message is-danger'
      msg.innerHTML = `<div class="message-body">
                            Se ${ss} ${bad} error${s}. Por favor ingrese valores numéricos para calcular el importe.
                        </div>`

      box.appendChild(msg)
    } else {
      msg.innerHTML = `<div class="message-body">
                            Se ${ss} ${bad} error${s}. Por favor ingrese valores numéricos para calcular el importe.
                        </div>`
    }
  }

  if (bad == 0 && msg != undefined) {
    box.removeChild(msg)
  }

  return bad
}
